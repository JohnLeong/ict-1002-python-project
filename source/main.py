from Tkinter import *
from classes.application import *

def main():
    """ The main loop of the program"""
    root = Tk()
    app = Application(master=root)
    app.mainloop()
    #root.destroy()

if __name__ == "__main__":
    main()
