from Tkinter import *
import ttk
import math
import tkMessageBox

class DataTable(Frame):
    def __init__(self, parent, dataset, max_rows = 10, column_width = 75):
        Frame.__init__(self, parent)
        self.current_page = 0
        self.dataset = dataset
        self.max_rows = max_rows
        self.search_results = []
        self.showing_search_results = False

        #Create the table using tkinter's treeview
        self.treeview = ttk.Treeview(self)
        self.treeview.grid(row = 1, column = 0, sticky = "w")
        self.treeview['show'] = 'headings'

        style = ttk.Style()
        style.configure("Treeview.Heading", font = ('Calibri', 9, 'bold'))
        style.configure("Treeview", font = ('Calibri', 9))

        if len(dataset) > 0:
            self.treeview["columns"] = dataset[0].keys()
            for key in dataset[0].keys():
                self.treeview.heading(key, text = key)
                self.treeview.column(key, width = column_width, stretch = False)

            for i in range(self.max_rows):
                self.treeview.insert("", i, i, values = ())

            self.update_treeview()

        #Create scrollbars
        self.horizontal_scrollbar = ttk.Scrollbar(self, orient = HORIZONTAL)
        self.horizontal_scrollbar.configure(command = self.treeview.xview)
        self.horizontal_scrollbar.grid(row = 2, column = 0, sticky = "we")
        self.treeview.configure(xscrollcommand = self.horizontal_scrollbar.set)

        self.vertical_scrollbar = ttk.Scrollbar(self, orient = VERTICAL)
        self.vertical_scrollbar.configure(command = self.treeview.yview)
        self.vertical_scrollbar.grid(row = 1, column = 1, sticky = "ns")
        self.treeview.configure(yscrollcommand = self.vertical_scrollbar.set)


        #Create search buttons
        self.search_frame = Frame(self)
        self.search_frame.grid(row = 0, column = 0, sticky = "w")
        self.search_lbl = Label(self.search_frame, text = "Search keywords below", font = ('Calibri', 9, 'bold'))
        self.search_lbl.grid(row = 0, column = 0, sticky="w")
        self.search_box = Entry(self.search_frame)
        self.search_box.grid(row = 1, column = 0, sticky="we")
        self.search_button = Button(self.search_frame, text="Search", font=('Calibri', 9),command=self.search_for_entry, width=10)
        self.search_button.grid(row = 1, column = 1, sticky="we")
        self.reset_search_button = Button(self.search_frame, text="Reset", font=('Calibri', 9),command=self.reset_search, width=10)
        self.reset_search_button.grid(row = 1, column = 2, sticky="we")

        self.page_selection_frame = Frame(self)
        self.page_selection_frame.grid(row = 3, column = 0, sticky = "w")

        self.results = Label(self.page_selection_frame, text = str(len(dataset)) + " Results Found!", font = ('Calibri', 9, 'bold'))
        self.results.grid(row = 0, column = 0, sticky = "w")

        self.buttons_frame = Frame(self.page_selection_frame)
        self.buttons_frame.grid(row = 1, column = 0, sticky = "w")
        self.left_button = Button(self.buttons_frame, text = "Prev page", font = ('Calibri', 9), command = self.prev_page, width = 15)
        self.left_button.grid(row = 0, column = 0, sticky = "we")
        self.right_button = Button(self.buttons_frame, text = "Next page", font = ('Calibri', 9), command = self.next_page, width = 15)
        self.right_button.grid(row = 0, column = 1, sticky = "we")
        self.page_label = Label(self.buttons_frame, text = "Page 1", font = ('Calibri', 9))
        self.page_label.grid(row = 0, column = 2, sticky = "we")

    def prev_page(self):
        """Displays the previous page in the table"""
        if self.current_page < 1:
            return
        self.current_page -= 1
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        self.update_treeview()

    def next_page(self):
        """Displays the next page in the table"""
        display_dataset_len = len(self.search_results) if self.showing_search_results else len(self.dataset)
        if self.current_page + 2 > int(math.ceil(float(display_dataset_len) / self.max_rows)):
            return
        self.current_page += 1
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        self.update_treeview()

    def update_treeview(self):
        """Updates the table to display the results from the current page"""
        x = self.treeview.get_children()
        display_dataset = self.search_results if self.showing_search_results else self.dataset
        for i in range(len(x)):
            try:
                if self.current_page * self.max_rows + i < len(display_dataset):
                    self.treeview.item(x[i], values = display_dataset[self.current_page * self.max_rows + i].values())
                else:
                    self.treeview.item(x[i], values = ())
            except UnicodeDecodeError:
                print "Unicode error"

    def search_for_entry(self):
        """Searches for a list of entries with the given keyword"""
        text = self.search_box.get().lower()
        if len(text) == 0:
            self.reset_search()
            return
        if len(text) < 2:
            tkMessageBox.showerror("Search error", "Keyword too short, please enter a keyword with more than 1 character")
            return
        self.showing_search_results = True
        #Clear list of old search results
        self.search_results[:] = []
        self.current_page = 0
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        for item in self.dataset:
            for val in item.values():
                if text in str(val).lower():
                    self.search_results.append(item)
        self.results["text"] = str(len(self.search_results)) + " Results Found!"
        self.update_treeview()

    def reset_search(self):
        self.showing_search_results = False;
        self.results["text"] = str(len(self.dataset)) + " Results Found!"
        self.current_page = 0
        self.page_label["text"] = "Page " + str(self.current_page + 1)
        self.update_treeview()
