from Tkinter import *
from data_analyser import *
from data_table import *
import tkFileDialog
import ttk
import os.path
import csv
import copy
import tkMessageBox

class Application(Frame):
    CONST_APP_NAME = "ICT1002 Python Project Grp 25: Government Procurement & Registered Companies Data Analytics Reader"
    CONST_WINDOW_MIN_WIDTH = 700
    CONST_WINDOW_MIN_HEIGHT = 300
    CONST_PATH_ICON = "icon/sit_icon.ico"
    CONST_BTN_HEAD_COL = "#0051e3"
    CONST_REQ_HEADERS_1 = ("agency", "tender_description", "award_date", "supplier_name", "awarded_amt")
    CONST_REQ_HEADERS_2 = ("company_name", "workhead", "grade", "expiry_date", "postal_code")

    def __init__(self, master=None):
        self.selectedFilePath_01 = "No file selected"
        self.selectedFilePath_02 = "No file selected"
        self.selected_export_path = "No path specified"
        self.dataset1 = []
        self.dataset2 = []
        self.data_analyser = DataAnalyser()
        self.process_data_finished = False
        self.current_frame_displayed = None
        self.all_display = None
        self.monthly_display = None
        self.total_procurement = None
        self.total_procurement_original = None
        self.total_procurement_display_increasing = None
        self.total_procurement_display_decreasing = None
        self.awarded_and_registered = None
        self.awarded_and_registered_display = None
        self.awarded_and_non_registered = None
        self.awarded_and_non_registered_display = None
        self.registered = None
        self.awarded_data = None
        self.reg_contractor_display = None
        self.non_reg_contractor_display = None
        self.registered_contractors = None
        self.postal_code_display_increasing = None
        self.postal_code_display_decreasing = None
        self.postal_sector_display = None
        self.category_display = None
        self.category_display2 = None
        self.expired = None
        self.expired_display = None
        self.active = None
        self.active_display = None
        self.list_contractors_workhead_display = None
        self.list_contractors_workhead = None
        self.contractors_gradecount = None
        self.contractors_gradecount_display = None

        Frame.__init__(self, master)
        master.title(Application.CONST_APP_NAME)
        master.minsize(Application.CONST_WINDOW_MIN_WIDTH, Application.CONST_WINDOW_MIN_HEIGHT)
        master.iconbitmap(Application.CONST_PATH_ICON)
        #master.resizable(False, False)
        self.pack(anchor = "w", padx = (10, 10), pady = (10, 10))
        self.create_widgets()

    def create_widgets(self):
        """Creates the program window and GUI using tkinter"""

        #Tab controls
        self.tab_control = ttk.Notebook(self)
        self.tab_control.grid(row = 0, column = 0, sticky = "w")

        #Change font for tab
        s = ttk.Style()
        s.configure('TNotebook.Tab', font = ('Calibri', 10, 'bold'))

        "----------------------------------------------------------------------------------------------------------"
        #File selection frame
        self.local_data_frame = Frame(self.tab_control)
        self.local_data_frame.grid(row = 0, column = 0, sticky = "w")
        self.local_data_frame.grid_rowconfigure(0, minsize = 10)
        self.tab_control.add(self.local_data_frame, text = "Read data from file")

        self.file_select_frame = LabelFrame(self.local_data_frame, text = "File selection", font = ('Calibri', 9, 'bold'), width = 800)
        self.file_select_frame.grid(row = 1, column = 0, sticky = "w")
        self.file_select_frame.grid_columnconfigure(3, minsize = 450)
        self.file_select_frame.grid_rowconfigure(0, minsize=10)

        self.file_button_01 = Button(self.file_select_frame, text = "Select a file", font = ('Calibri', 9), command = self.select_file_01, width = 14)
        self.file_button_01.grid(row = 1, column = 0, sticky = "w", padx = 10)
        self.file_label_01 = Label(self.file_select_frame, text = "Procurement Dataset: ", font = ('Calibri', 9))
        self.file_label_01.grid(row = 1, column = 1, sticky = "w")
        self.selected_file_label_01 = Label(self.file_select_frame, text = self.selectedFilePath_01, font = ('Calibri', 9), anchor = "w")
        self.selected_file_label_01.grid(row = 1, column = 3, sticky = "w")

        self.file_button_02 = Button(self.file_select_frame, text = "Select a file", font = ('Calibri', 9), command = self.select_file_02, width = 14)
        self.file_button_02.grid(row = 2, column = 0, sticky = "w", padx = 10)
        self.file_label_02 = Label(self.file_select_frame, text = "Contractors Dataset: ", font = ('Calibri', 9))
        self.file_label_02.grid(row = 2, column = 1, sticky = "w")
        self.selected_file_label_02 = Label(self.file_select_frame, text = self.selectedFilePath_02, font = ('Calibri', 9), anchor = "w")
        self.selected_file_label_02.grid(row = 2, column = 3, sticky = "w")

        self.export_path_button = Button(self.file_select_frame, text = "Select path", font = ('Calibri', 9), command = self.select_export_path, width = 14)
        self.export_path_button.grid(row = 3, column = 0, sticky = "w", padx = 10)
        self.export_path_label = Label(self.file_select_frame, text = "Export path: ", font = ('Calibri', 9))
        self.export_path_label.grid(row = 3, column = 1, sticky = "w")
        self.export_path_label = Label(self.file_select_frame, text = self.selected_export_path, font = ('Calibri', 9), anchor = "w")
        self.export_path_label.grid(row = 3, column = 3, sticky = "w")

        self.file_select_frame.grid_rowconfigure(4, minsize = 10)
        self.process_button = Button(self.file_select_frame, text = "Process Data", font = ('Calibri', 9), command = self.process_file_data, width = 14)
        self.process_button.grid(row = 5, column = 0, sticky = "w", padx = 10, pady = 10)

        #Dataset functions frame
        self.local_data_frame.grid_rowconfigure(2, minsize = 20)
        self.func_data_frame = Frame(self.local_data_frame)
        self.func_data_frame.grid(row = 3, column = 0, sticky = "w")

        self.functions_frame = LabelFrame(self.func_data_frame, text = "Dataset functions", font = ('Calibri', 9, 'bold'))
        self.functions_frame.grid(row = 0, column = 0, sticky = "wne")

        self.button_canvas = Canvas(self.functions_frame, width = 300, height = 250)
        self.button_scrollbar = Scrollbar(self.functions_frame, orient="vertical", command=self.button_canvas.yview)
        self.button_frame = Frame(self.button_canvas)
        self.button_canvas.create_window(0, 0, anchor = 'nw', window = self.button_frame)

        Label(self.button_frame, text = "All/Monthly", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 0, column = 0, sticky = "e")
        Button(self.button_frame, text = "View all procurements", font = ('Calibri', 9), command = self.display_procurement, width = 48).grid(row = 1, column = 0, sticky = "w")
        Button(self.button_frame, text = "View monthly procurements", font = ('Calibri', 9), command = self.monthly_procurement, width = 48).grid(row = 2, column = 0, sticky = "w")

        Label(self.button_frame, text = "Total procurements", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 3, column = 0, sticky = "e")
        Button(self.button_frame, text = "List original total procurements", font = ('Calibri', 9), command = self.list_procurements_original, width = 48).grid(row = 4, column = 0, sticky="w")
        Button(self.button_frame, text = "List increasing total procurements", font = ('Calibri', 9), command = self.list_procurements_increasing, width = 48).grid(row = 5, column = 0, sticky = "w")
        Button(self.button_frame, text = "List decreasing total procurements", font = ('Calibri', 9), command = self.list_procurements_decreasing, width = 48).grid(row = 6, column = 0, sticky = "w")

        Label(self.button_frame, text = "Registered/Non-Registered contractors", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 7, column = 0, sticky = "e")
        Button(self.button_frame, text = "List awarded registered contractors", font = ('Calibri', 9), command = self.list_awarded_registered, width = 48).grid(row = 8, column = 0, sticky =" w")
        Button(self.button_frame, text = "List awarded non-registered contractors", font = ('Calibri', 9), command = self.list_awarded_non_registered, width = 48).grid(row = 9, column = 0, sticky =" w")
        Button(self.button_frame, text = "View top registered procurement", font = ('Calibri', 9), command = self.list_top_registered, width = 48).grid(row = 10, column = 0, sticky = "w")
        Button(self.button_frame, text = "View top non-registered procurement", font = ('Calibri', 9), command = self.list_top_non_registered, width = 48).grid(row = 11, column = 0, sticky = "w")

        Label(self.button_frame, text = "Categorized", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 12, column = 0, sticky = "e")
        Button(self.button_frame, text = "Categorise government spendings", font = ('Calibri', 9), command = self.display_categories, width = 48).grid(row = 13, column = 0, sticky = "w")

        Label(self.button_frame, text = "Postal Sector", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 14, column = 0, sticky = "e")
        Button(self.button_frame, text = "List registered contractors by increasing postal code", font = ('Calibri', 9), command = self.list_registered_postal_code_increasing, width = 48).grid(row = 15, column = 0, sticky = "w")
        Button(self.button_frame, text = "List registered contractors by decreasing postal code", font = ('Calibri', 9), command = self.list_registered_postal_code_decreasing, width = 48).grid(row = 16, column = 0, sticky = "w")
        Button(self.button_frame, text = "View top postal sectors (first 2 digits of postal code)", font= ('Calibri', 9), command = self.list_top_postal_sectors, width = 48).grid(row = 17, column = 0, sticky = "w")

        Label(self.button_frame, text = "Expiry Date", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 18, column = 0, sticky = "e")
        Button(self.button_frame, text = "View Expired Sectors", font= ('Calibri', 9), command = self.listing_expired_contractors, width = 48).grid(row = 19, column = 0, sticky = "w")
        Button(self.button_frame, text = "View Active Sectors", font= ('Calibri', 9), command = self.listing_active_contractors, width = 48).grid(row = 20, column = 0, sticky = "w")

        Label(self.button_frame, text = "Wordhead/Grade", font = ('Calibri', 9, 'bold'), width = 48, fg = Application.CONST_BTN_HEAD_COL).grid(row = 21, column = 0, sticky = "e")
        Button(self.button_frame, text = "Workhead Count", font= ('Calibri', 9), command = self.list_contractors_in_workhead, width = 48).grid(row = 22, column = 0, sticky = "w")
        Button(self.button_frame, text = "Contractors Gradecount", font= ('Calibri', 9), command = self.list_contractor_gradecount, width = 48).grid(row = 23, column = 0, sticky = "w")

        self.button_canvas.update_idletasks()
        self.button_canvas.configure(scrollregion = self.button_canvas.bbox('all'), yscrollcommand = self.button_scrollbar.set)

        self.button_canvas.grid(row = 0, column = 0, sticky = "w")
        self.button_scrollbar.grid(row = 0, column = 1, sticky = "ns")

        #Export functions frame
        self.export_functions_frame = LabelFrame(self.func_data_frame, text = "Export functions", font = ('Calibri', 9, 'bold'))
        self.export_functions_frame.grid(row = 1, column = 0, sticky = "wne")
        self.export_procurement_button = Button(self.export_functions_frame, text = "Export procurement",font = ('Calibri', 9), command = self.export_agency_procurement)
        self.export_procurement_button.grid(row = 0, column = 0, sticky = "w")

        #Display frame
        self.display_frame = LabelFrame(self.func_data_frame, text = "Data display", font = ('Calibri', 9, 'bold'), width = 450, height = 200)
        self.display_frame.grid(row = 0, column = 2, rowspan = 2, sticky = "wn")

        "----------------------------------------------------------------------------------------------------------"
        #Project information frame
        self.project_information_frame = Frame(self.tab_control)
        self.project_information_frame.grid(row = 0, column = 0, sticky = "w")
        self.project_information_frame.grid_rowconfigure(0, minsize = 10)
        self.tab_control.add(self.project_information_frame, text = "Project Information")

        self.display_dataset_info_frame = LabelFrame(self.project_information_frame, text= "What dataset did we incorporate into this application?", font = ('Calibri', 10, 'bold'))
        self.display_dataset_info_frame.grid(row = 1, column = 0)
        self.dataset1_title = Label(self.display_dataset_info_frame, text = "Dataset 1: Government Procurement Dataset", font = ('Calibri', 10, 'bold'))
        self.dataset1_title.grid(row = 0, column = 0, sticky = "w")
        self.dataset1_info = Label(self.display_dataset_info_frame, text = "The Government Procurement dataset is the first dataset used, and mainly consists of data pertaining to official government tenders to both registered and non-registered contractors.\nThe listed contractor/supplier names are either individual entities, or a joint collaboration between different contractors. The dataset contains the tender number, status and description of the respective tenders.\nFurthermore, it includes the agency each tender belongs to, the name of the contractor the tenders are assigned to, if any. It also lists the awarded amount and dates of each tender.", font = ('Calibri', 10), justify=LEFT)
        self.dataset1_info.grid(row = 1, column = 0, sticky = "w")
        self.dataset1_title = Label(self.display_dataset_info_frame, text="Dataset 2: List of Registered Contractors dataset", font=('Calibri', 10, 'bold'))
        self.dataset1_title.grid(row = 2, column = 0, sticky="w")
        self.dataset2_info = Label(self.display_dataset_info_frame,text = "The List of Registered Contractors dataset is the second dataset used, and largely consists of data related to the individual government registered contractors.\nThe dataset contains data such as the name of the contractor, the Unique Entity Number (UEN), the workhead, the grade and the expiry date of the contract.\nIt also includes additional information pertaining to the individual contracts.\nContractor address and contact information such as street name, building number, unit number, building name, postal code and telephone number are included in the dataset as well.",font = ('Calibri', 10), justify = LEFT)
        self.dataset2_info.grid(row = 3, column=0, sticky="w")
        self.display_team_members_frame = LabelFrame(self.project_information_frame, text = "Group 25 Members", font = ('Calibri', 10, 'bold'))
        self.display_team_members_frame.grid(row = 4, column = 0, sticky =  "w")
        self.grp_member_info = Label(self.display_team_members_frame, text = "This project is done by the dedicated members of Group 25:", font = ('Calibri', 10))
        self.grp_member_info.grid(row = 0, column = 0, sticky = "w")
        self.grp_member_1 = Label(self.display_team_members_frame, text = "Group Member 1: JERONE POH YONG CHENG", font = ('Calibri', 10))
        self.grp_member_1.grid(row = 1, column = 0, sticky = "w")
        self.grp_member_2 = Label(self.display_team_members_frame, text = "Group Member 2: FARIN TAHRIMA RAHMAN", font = ('Calibri', 10))
        self.grp_member_2.grid(row = 2, column = 0, sticky = "w")
        self.grp_member_3 = Label(self.display_team_members_frame, text = "Group Member 3: LEONG JHUN SHING JOHN", font = ('Calibri', 10))
        self.grp_member_3.grid(row = 3, column = 0, sticky = "w")
        self.grp_member_4 = Label(self.display_team_members_frame, text="Group Member 4: JARED TAN JING JIE", font = ('Calibri', 10))
        self.grp_member_4.grid(row = 4, column = 0, sticky = "w")
        self.grp_member_5 = Label(self.display_team_members_frame, text = "Group Member 5: LIM CHONG JING", font = ('Calibri', 10))
        self.grp_member_5.grid(row = 5, column = 0, sticky = "w")
        self.grp_member_6 = Label(self.display_team_members_frame, text="Group Member 6: NG DE EN GORDON", font = ('Calibri', 10))
        self.grp_member_6.grid(row = 6, column = 0, sticky = "w")
        self.grp_member_copyright = Label(self.display_team_members_frame, text = "Copyright" + u"\u00a9" + "2019 Singapore Institute of Technology Group 25", font = ('Calibri', 10))
        self.grp_member_copyright.grid(row = 7, column = 0, sticky = "w")

    def select_file_01(self):
        """Displays a popup for the user to select the pocurement dataset file from their computer"""

        self.selectedFilePath_01 = tkFileDialog.askopenfilename(initialdir = "/", title = "Select file", filetypes = (("csv files","*.csv"),("all files","*.*")))
        self.selectedFilePath_01 = "No file selected" if len(self.selectedFilePath_01) < 1 else self.selectedFilePath_01
        self.selected_file_label_01["text"] = self.selectedFilePath_01

    def select_file_02(self):
        """Displays a popup for the user to select the contractors dataset file from their computer"""

        self.selectedFilePath_02 = tkFileDialog.askopenfilename(initialdir = "/", title = "Select file", filetypes = (("csv files","*.csv"),("all files","*.*")))
        self.selectedFilePath_02 = "No file selected" if len(self.selectedFilePath_02) < 1 else self.selectedFilePath_02
        self.selected_file_label_02["text"] = self.selectedFilePath_02

    def select_export_path(self):
        """Displays a popup for the user to select the export path"""

        self.selected_export_path = tkFileDialog.askdirectory()
        self.selected_export_path = "No path specified" if len(self.selected_export_path) < 1 else self.selected_export_path
        self.export_path_label["text"] = self.selected_export_path

    def process_file_data(self):
        """Processes the data in the 2 provided datasets and stores them in 2 lists"""

        #Checks if the given file paths are valid
        if not(os.path.exists(self.selectedFilePath_01)) or not(os.path.exists(self.selectedFilePath_02)):
            tkMessageBox.showerror("Error processing data", "Invalid file path!")
            return False

        self.read_filedata_to_list(self.selectedFilePath_01, self.dataset1)
        self.read_filedata_to_list(self.selectedFilePath_02, self.dataset2)

        #Check for all required columns
        for header in Application.CONST_REQ_HEADERS_1:
            if header not in self.dataset1[0].keys():
                tkMessageBox.showerror("Error processing data", "Required header: " + header + " , not found in file!")
                return False
        for header in Application.CONST_REQ_HEADERS_2:
            if header not in self.dataset2[0].keys():
                tkMessageBox.showerror("Error processing data", "Required header: " + header + " , not found in file!")
                return False

        #Create a new data table based on dataset 1
        self.all_display = DataTable(self.display_frame, self.dataset1, 20, 95)

        self.process_data_finished = True
        return True

    def read_filedata_to_list(self, filepath, target_list):
        """Reads the file at the specified file path and stores the data in target_list"""

        print "Reading file at " + filepath

        #Check for a valid file path
        if not(os.path.exists(filepath)):
            return False
        target_list[:] = []
        #Reads file line by line and stores the data in target_list
        #Values in the datasets are seperated by commas
        with open(filepath) as csvfile:
            read_csv = csv.reader(csvfile, delimiter=',')

            #Read the first line to form the column categories
            categories = next(read_csv)
            for line in read_csv:
                container = {}
                for i in range(len(categories)):
                    container[categories[i]] = line[i]
                target_list.append(container)

        print "Finished reading file"
        return True

    def set_display_frame(self, new_frame):
        """Sets the current frame in the 'Data display frame' to new_frame"""
        if self.current_frame_displayed:
            self.current_frame_displayed.grid_forget()
        self.current_frame_displayed = new_frame
        self.current_frame_displayed.grid(row = 0, column = 0)

    def display_procurement(self):
        """Displays every single data in dataset1 using the DataTable class"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        self.set_display_frame(self.all_display)

    def export_agency_procurement(self):
        """Exports each agency's procurement into their own text file(Function 2)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error exporting", "Data not found. Please input datasets!")
            return
        if not os.path.exists(self.selected_export_path):
            tkMessageBox.showerror("Error exporting", "Invalid export path!")
            return
        self.data_analyser.export_agency_procurement(self.dataset1, self.selected_export_path)

    def list_procurements_original(self):
        """Display total procurements for each government sectors in an original, unsorted order (Function 3a)"""
        if not self.process_data_finished:

            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.total_procurement_original is None:
            self.dataset1 = []  #reset dataset and retrieve original set again
            self.read_filedata_to_list(self.selectedFilePath_01, self.dataset1)
            self.total_procurement = self.data_analyser.list_procurements(self.dataset1)
            self.total_procurement_original = DataTable(self.display_frame, self.total_procurement[0], 10, 200)
        self.set_display_frame(self.total_procurement_original)
        self.process_data_finished = True
        return True

    def list_procurements_increasing(self):
        """Display total procurements for each government sectors that are sorted in increasing AND decreasing order (Function 3b)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.total_procurement_display_increasing is None:
            self.dataset1 = []  #reset dataset and retrieve original set again
            self.read_filedata_to_list(self.selectedFilePath_01, self.dataset1)
            self.total_procurement = self.data_analyser.list_procurements(self.dataset1)
            self.total_procurement_display_increasing = DataTable(self.display_frame, self.total_procurement[1], 10, 200)
        self.set_display_frame(self.total_procurement_display_increasing)
        self.process_data_finished = True
        return True

    def list_procurements_decreasing(self):
        """Display total procurements for each government sectors in increasing AND decreasing order (Function 3b)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.total_procurement_display_decreasing is None:
            self.dataset1 = []  #reset dataset and retrieve original set again
            self.read_filedata_to_list(self.selectedFilePath_01, self.dataset1)
            self.total_procurement = self.data_analyser.list_procurements(self.dataset1)
            self.total_procurement_display_decreasing = DataTable(self.display_frame, self.total_procurement[2], 10, 200)
        self.set_display_frame(self.total_procurement_display_decreasing)
        self.process_data_finished = True
        return True

    def list_awarded_registered(self):
        """List the awarded vendors which are the registered contractors  (Function 4a)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.awarded_and_registered_display is None:
            self.awarded_and_registered = self.data_analyser.list_awarded_registered(self.dataset1, self.dataset2)
            self.awarded_and_registered_display = DataTable(self.display_frame, self.awarded_and_registered[0], 10, 70)
        self.set_display_frame(self.awarded_and_registered_display)
        self.process_data_finished = True
        return True

    def list_awarded_non_registered(self):
        """List the awarded vendors which are the non-registered contractors (Function 4b)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.awarded_and_non_registered_display is None:
            self.awarded_and_non_registered = self.data_analyser.list_awarded_registered(self.dataset1, self.dataset2)
            self.awarded_and_non_registered_display = DataTable(self.display_frame, self.awarded_and_non_registered[1], 10, 100)
        self.set_display_frame(self.awarded_and_non_registered_display)
        self.process_data_finished = True
        return True

    def list_top_registered(self):
        """Lists (table) procurement awarded to registered contractors  (Function 5)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.reg_contractor_display is None:
            self.awarded_data = self.data_analyser.list_awarded_contractors(self.get_dataset_deepcopy(self.dataset1), self.get_dataset_deepcopy(self.dataset2))
            self.reg_contractor_display = DataTable(self.display_frame, self.awarded_data[0], 10, 200)
        self.set_display_frame(self.reg_contractor_display)
        self.process_data_finished = True
        return True

    def list_top_non_registered(self):
        """Lists (table) procurement awarded to non registered contractors (ordered by top procurement) (Function 5)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.non_reg_contractor_display is None:
            self.awarded_data = self.data_analyser.list_awarded_contractors(self.get_dataset_deepcopy(self.dataset1), self.get_dataset_deepcopy(self.dataset2))
            self.non_reg_contractor_display = DataTable(self.display_frame, self.awarded_data[1], 10, 200)
        self.set_display_frame(self.non_reg_contractor_display)
        self.process_data_finished = True
        return True

    def display_categories(self):
        """Displays the categories and spending as a list to parse into DataTable Class (Function 6)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        results = self.data_analyser.categorise_govt_spending(self.dataset1)
        self.category_display = DataTable(self.display_frame, results, 10, 150)
        self.set_display_frame(self.category_display)

    def list_registered_postal_code_increasing(self):
        """List down the registered contractors according based on their postal code in increasing order (Function 7a)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.postal_code_display_increasing is None:
            self.registered_contractors = self.data_analyser.list_registered_postal_code(self.get_dataset_deepcopy(self.dataset2))
            self.postal_code_display_increasing = DataTable(self.display_frame, self.registered_contractors[0], 10, 125)
        self.set_display_frame(self.postal_code_display_increasing)
        self.process_data_finished = True
        return True

    def list_registered_postal_code_decreasing(self):
        """List down the registered contractors according based on their postal code in decreasing order (Function 7a)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.postal_code_display_decreasing is None:
            self.registered_contractors = self.data_analyser.list_registered_postal_code(self.get_dataset_deepcopy(self.dataset2))
            self.postal_code_display_decreasing = DataTable(self.display_frame, self.registered_contractors[1], 10, 125)
        self.set_display_frame(self.postal_code_display_decreasing)
        self.process_data_finished = True
        return True

    def list_top_postal_sectors(self):
        """List down the top postal sectors (first 2 digits of postal code) and its number of contractors belong to it (Function 7b) """
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.postal_sector_display is None:
            self.registered_contractors = self.data_analyser.list_top_postal_sectors(self.get_dataset_deepcopy(self.dataset2))
            self.postal_sector_display = DataTable(self.display_frame, self.registered_contractors, 10, 200)
        self.set_display_frame(self.postal_sector_display)
        self.process_data_finished = True
        return True

    def monthly_procurement(self):
        """Displays the procurement for every month, and the prediction for the next X months (Function 8)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        self.monthly_display = self.data_analyser.list_monthly_procurement(self.dataset1, self.display_frame)

        self.set_display_frame(self.monthly_display)

    def listing_expired_contractors(self):
        """List down the expired contractors, the expiry dates after current date (Function 11a)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.expired_display is None:
            self.expired = self.data_analyser.list_expired_contractors(self.dataset2)
            self.expired_display = DataTable(self.display_frame, self.expired[0], 10, 80)
        self.set_display_frame(self.expired_display)
        self.process_data_finished = True
        return True

    def listing_active_contractors(self):
        """List down the expired contractors, the expiry dates before current date (Function 11b)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.active_display is None:
            self.active = self.data_analyser.list_expired_contractors(self.dataset2)
            self.active_display = DataTable(self.display_frame, self.active[1], 10, 80)
        self.set_display_frame(self.active_display)
        self.process_data_finished = True
        return True

    def list_contractors_in_workhead(self):
        """List down the number of contractors in each workhead (Function 9)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.list_contractors_workhead_display is None:
            self.list_contractors_workhead = self.data_analyser.list_contractorcount_by_workhead(self.dataset2)
            self.list_contractors_workhead_display = DataTable(self.display_frame, self.list_contractors_workhead, 15, 100)
        self.set_display_frame(self.list_contractors_workhead_display)
        self.process_data_finished = True
        return True

    def list_contractor_gradecount(self):
        """List down the number of contractors for each grade (Function 10)"""
        if not self.process_data_finished:
            tkMessageBox.showerror("Error displaying data", "Data not found. Please input datasets!")
            return
        if self.contractors_gradecount_display is None:
            self.contractors_gradecount = self.data_analyser.list_contractorcount_by_grade(self.dataset2)
            self.contractors_gradecount_display = DataTable(self.display_frame, self.contractors_gradecount, 15, 100)
        self.set_display_frame(self.contractors_gradecount_display)
        self.process_data_finished = True
        return True

    def get_dataset_deepcopy(self, dataset):
        """Creates a deepcopy of the specified dataset

        Parameters:
        dataset (list):    The target dataset to deeocopy
        """
        new_dataset = []
        #Creates a deepcopy of every dictionary in dataset
        for row in dataset:
            new_dataset.append(copy.deepcopy(row))
        return new_dataset
