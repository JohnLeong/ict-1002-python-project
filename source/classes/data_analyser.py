from Tkinter import *
import sys
import os.path
import re
import operator
import ttk
import collections
import datetime
#Install matplotlib to prevent errors
try:
    import matplotlib
    matplotlib.use("TkAgg")
    from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
    from matplotlib.figure import Figure
except ImportError:
    print "MatPlotLib not installed"

class DataAnalyser():
    def __init__(self):
        pass

    def create_folder_at_directory(self, folder_name, directory):
        """Creates a folder at the specified directory

        Parameters:
        folder_name (string):   The name of the folder to create
                                If the folder already exists, a number will be added
                                to the back of the folder name

        directory (list):       The directory to create the folder in
        """

        new_folder_name = "/" + folder_name
        num = 1
        while os.path.exists(directory + new_folder_name):
            new_folder_name = "/Agency Procurement_" + str(num)
            num += 1
        os.mkdir(directory + new_folder_name)
        return directory + new_folder_name

    def export_agency_procurement(self, dataset1, export_path):
        """Exports each agency's procurement into their own text file(Function 2)

        Parameters:
        dataset1 (list):        The government agency procurement data set

        export_path (string):   The directory to export the text files to
        """

        #Goes through dataset1 by row and sorts them according to agency
        agencies = {}
        for data in dataset1:
            if data["agency"] not in agencies.keys():
                agencies[data["agency"]] = []
            agencies[data["agency"]].append(data)

        #Create a folder at the specified export path
        folder_path = self.create_folder_at_directory("Agency Procurement", export_path)

        for key, value in agencies.items():
            #Removes invalid symbols in agency names
            file_name = "/" + re.sub('[^A-Za-z0-9]+', '', key) + ".txt"
            f = open(folder_path + file_name, "w+")
            f.write("Procurement for the agency of: " + key + "\n")
            total_amt = 0
            for item in value:
                try:
                    total_amt += int(item["awarded_amt"])
                except ValueError:
                    pass
            f.write("Total awarded amount: " + str(total_amt) + "\n\n")
            #Write to file
            for item in value:
                f.write("Tender no.\t: " + item["tender_no."] + "\n")
                f.write("Award date\t: " + item["award_date"] + "\n")
                f.write("Awarded amt\t: " + item["awarded_amt"] + "\n")
                f.write("Supplier\t: " + item["supplier_name"] + "\n")
                f.write("Detail status\t: " + item["tender_detail_status"] + "\n")
                f.write("Description\t: " + item["tender_description"] + "\n\n")
            f.close()

    def list_procurements(self, dataset1):
        """Display number of procurements from each government sector in increasing AND decreasing order. (Function 3)

        Parameters:
        dataset1 (list):    The government agency procurement data set
        """

        agencies = {}
        total_procurements = {}

        for data in dataset1:
            if data["agency"] not in agencies.keys():
                agencies[data["agency"]] = []
            agencies[data["agency"]].append(data)

        for key, value in agencies.items(): #creates new dictionary where key is the procurement_name and value will be quantity of procurements
            for item in value:
                if item["agency"] not in total_procurements:
                    total_procurements[item["agency"]] = 1
                else:
                    total_procurements[item["agency"]] += 1

        unsorted_procurements = [{"procurement_name": key, "quantity": value} for key, value in total_procurements.items()]
        sorted_tuple_increasing = sorted(total_procurements.items(), key = operator.itemgetter(1), reverse = False)     #sort in increasing
        sorted_procurements_increasing = [{"procurement_name": agency[0], "quantity": agency[1]} for agency in sorted_tuple_increasing]
        sorted_tuple_decreasing = sorted(total_procurements.items(), key = operator.itemgetter(1), reverse = True)      #sort in decreasing
        sorted_procurements_decreasing = [{"procurement_name": agency[0], "quantity": agency[1]} for agency in sorted_tuple_decreasing]
        return unsorted_procurements, sorted_procurements_increasing, sorted_procurements_decreasing

    def list_awarded_registered(self, dataset1, dataset2):
        """List down the awarded vendors which are the registered contractors. (Function 4)

         Parameters:
         dataset1 (list):    The government agency procurement data set
         dataset2 (list):    The listing of registered contractors data set
         """
        total_dict = {}
        for data in dataset1:
            if float(data["awarded_amt"]) <= 0:  # to check if awarded amt exists, skip if 0
                continue
            else:
                dict1 = {data["supplier_name"]: data}  # if there is awarded amt, to update into total_dict with supplier_name as key
                total_dict.update(dict1)

        registered_vendors = []
        non_registered_vendors = []
        registered_dict = {}

        for data in dataset2:
            dict1 = {data['company_name']: data}  # to update into registered_dict with company_name as key
            registered_dict.update(dict1)

        for name in total_dict.keys():
            if name in registered_dict.keys():  # compare whether supplier is a registered vendor
                registered_vendors.append(registered_dict[name])
            elif name not in registered_dict.keys():
                non_registered_vendors.append(total_dict[name])
        return registered_vendors, non_registered_vendors

    def awarded_sort_key(self, i):  # sort key for list_awarded_contractors()
        return float(i.values()[0])

    def list_awarded_contractors(self, dataset1, dataset2):
        """List down the awarded vendors which are the registered contractors. (Function 5)

        Parameters:
        dataset1 (list):    The government agency procurement data set
        dataset2 (list):    The government agency list of registered contractors
        """
        registered_list = []
        non_registered_list = []

        hybriddict = {}
        for i in dataset1:  #set name as key and data as value
            i.pop("tender_no.", None)   #removes unnecessary columns
            i.pop("award_date", None)
            i.pop("tender_description", None)
            i.pop("tender_detail_status", None)
            i.pop("agency", None)
            dict = {i['supplier_name'].upper() : i}
            hybriddict.update(dict)

        regdict = {}
        for i in dataset2:  # set name as key and data as value
            dict = {i['company_name'].upper(): i}
            regdict.update(dict)

        for name in hybriddict.keys():
            if name in regdict.keys():  # compare and sort/filter non/registered
                registered_list.append(hybriddict[name])  # found
            else:
                non_registered_list.append(hybriddict[name])  # not found
        #sort list based on procurement value, highest first
        registered_list = sorted(registered_list, key=self.awarded_sort_key, reverse=True)
        non_registered_list = sorted(non_registered_list, key=self.awarded_sort_key, reverse=True)

        return registered_list, non_registered_list

    def categorise_govt_spending(self, dataset1):
        """Identify one way to categorize the government agency and see how much money the government spend on each category (Function 6)
        Parameters:
        dataset1 (list):    The government agency procurement data set
        """
        categories = {"Institute of Higher Learning": 0,"Defence": 0, "Transport": 0, "Schools": 0, "Ministry": 0, "Polytechnics": 0,
                      "Technology":0, "Judiciary Courts":0, "Workforce":0, "SkillsFuture":0, "Public Sector":0, "Authorities":0,
                      "Labour Sector":0, "Colleges":0, "Accounting and Finance":0, "Istana":0, "Parliament":0, "Religious Groups":0,
                      "SPRING Singapore":0, "Health Sector":0, "Culture and Heritage Sector":0, "Miscellaneous":0}
        agencies = {}
        categorieslist = []  # Creates Categories list to store Dictionaries of category and their spending
        # This prints out all the agency names and their awarded amounts without duplicates in "government pocurement via gebiz"
        for item in dataset1:
            if item["agency"] not in agencies.keys():
                agencies[item["agency"]] = float(item["awarded_amt"])
            else:  # for all duplicate agencies
                agencies[item["agency"]] += float(item["awarded_amt"])

        # Categorizing the agencies by keywords and adding their total cost to each category
        for k in agencies:

            if "Institute" in k:
                categories["Institute of Higher Learning"] += agencies[k]
            if "Defence" in k:
                categories["Defence"] += agencies[k]
            if "Transport" in k:
                categories["Transport"] += agencies[k]
            if "Ministry" in k:
                categories["Ministry"] += agencies[k]
            if "Polytechnic" in k:
                categories["Polytechnics"] += agencies[k]
            if "School" in k:
                categories["Schools"] += agencies[k]
            if "Technology" in k:
                categories["Technology"] += agencies[k]
            if "courts" or "court" in k:
                categories["Judiciary Courts"] += agencies[k]
            if "Workforce" in k:
                categories["Workforce"] += agencies[k]
            if "SkillsFuture" in k:
                categories["SkillsFuture"] += agencies[k]
            if "public" and not "polytechnic" in k:
                categories["Public Sector"] += agencies[k]
            if "Authority" in k:
                categories["Authorities"] += agencies[k]
            if "Labour" in k:
                categories["Labour Sector"] += agencies[k]
            if "College" in k:
                categories["Colleges"] += agencies[k]
            if "Account" or "Finance" in k:
                categories["Accounting and Finance"] += agencies[k]
            if "Istana" in k:
                categories["Istana"] += agencies[k]
            if "Parliament" in k:
                categories["Parliament"] += agencies[k]
            if "SPRING" in k:
                categories["SPRING Singapore"] += agencies[k]
            if "Health" in k:
                categories["Health Sector"] += agencies[k]
            if "Islam" or "Muslim" or "Christian" or "Catholic" or "Taoist" or "Buddhism" or "Hinduism" or "Sikhism" in k:
                categories["Religious Groups"] += agencies[k]
            if "Culture" or "Heritage" in k:
                categories["Culture and Heritage Sector"] += agencies[k]
            else:
                categories["Miscellaneous"] += agencies[k]

        for i in categories.keys(): #Storing Dictionaries into CategoryList, max 2 decimal place ($)
            categorieslist.append({"Spending($)": format(categories[i],'.2f'), "Categories": i})

        return sorted(categorieslist) #returns a sorted list of categories and their spending

    def list_registered_postal_code(self, dataset2):

        """List down the registered distinct contractors according based on their postal code (Function 7a)

        Parameters: dataset2 (list):    The government agency list of registered contractors
        """

        for registered_info in dataset2:  # Remove unnecessary columns
            try:
                registered_info.pop('grade')
                registered_info.pop('uen_no')
                registered_info.pop('additional_info')
                registered_info.pop('workhead')
                registered_info.pop('expiry_date')
            except KeyError:
                print "Column already deleted!"

            if len(registered_info[
                       'postal_code']) == 5:  # Some of postal code has only 5 digits, so add a digit 0 infront, in line with normal Singapore postal code of 6 digits
                registered_info['postal_code'] = "0" + registered_info['postal_code']

            registered_info['company_name'] = registered_info[
                'company_name'].upper()  # standardise company name, street name and building name to capital letters
            registered_info['street_name'] = registered_info['street_name'].upper()
            registered_info['building_name'] = registered_info['building_name'].upper()

        non_duplicated_company_names = set()  # Remove duplicated company names
        registered_companies = []

        for registered_dict in dataset2:
            if registered_dict['company_name'] not in non_duplicated_company_names:
                non_duplicated_company_names.add(registered_dict['company_name'])
                registered_companies.append(registered_dict)

        sorted_increasing = sorted(registered_companies, key=operator.itemgetter('postal_code'), reverse=False)
        sorted_decreasing = sorted(registered_companies, key=operator.itemgetter('postal_code'), reverse=True)

        return sorted_increasing, sorted_decreasing

    def list_top_postal_sectors(self, dataset2):

        """List down the top postal sectors (first 2 digits of postal code) and its number of distinct contractors belonging to it (Function 7b)

        Parameters: dataset2 (list):    The government agency list of registered contractors
        """

        postal_sectors = {}

        for registered_info in dataset2:  # Remove unnecessary columns
            if len(registered_info[
                       'postal_code']) == 5:  # Some of postal code has only 5 digits, so add a digit 0 infront, in line with normal Singapore postal code of 6 digits
                registered_info['postal_code'] = "0" + registered_info['postal_code']

        non_duplicated_company_names = set()  # Filter out the duplicated company names
        registered_companies = []

        for registered_dict in dataset2:
            if registered_dict['company_name'] not in non_duplicated_company_names:  # Check if there are no duplicates from the names belonging to the set
                non_duplicated_company_names.add(
                    registered_dict['company_name'])  # Those non-duplicated names will be added to a set
                registered_companies.append(registered_dict)  # Add the whole dictionary into the list

        for registered_info in registered_companies:
            if registered_info['postal_code'][:2] not in postal_sectors:  # Check if first 2 digits of postal code is present in dictionary, if its not present, create a new key and value
                postal_sectors[registered_info['postal_code'][:2]] = 1
            else:
                postal_sectors[registered_info['postal_code'][:2]] += 1

        top_postal_sectors_tuple = sorted(postal_sectors.items(), key=operator.itemgetter(1),
                                          reverse=True)  # sort top postal sectors quantity in decreasing order
        top_postal_sectors_dict = [{"postal_sector (first 2 digits of postal code)": registered[0],
                                    "no_of_distinct_registered_contractors": registered[1]} for registered in
                                   top_postal_sectors_tuple]  # convert list of tuples to dictionary

        return top_postal_sectors_dict


    def list_monthly_procurement(self, dataset1, frame_parent):
        """Displays the procurement for every month, and the prediction for the next X months
        using a matplotlib graph and a tkinter listbox (Function 8)

        Parameters:
        dataset1 (list):        The government agency procurement data set
        frame_parent (Frame):   The frame parent for the monthly procurement frame
        """

        frame = Frame(frame_parent)
        list_box_frame = Frame(frame)
        list_box_frame.grid(row = 0, column = 0, sticky = "nw")
        Label(list_box_frame, text = "List view", font = ('Calibri', 9, 'bold')).grid(row = 0, column = 0, sticky = "w")
        monthly_listbox = Listbox(list_box_frame, width = 25, height = 15, font = ('Calibri', 9))
        monthly_listbox.grid(row = 1, column = 0, sticky = "nw")

        vertical_scrollbar = ttk.Scrollbar(list_box_frame, orient = VERTICAL)
        vertical_scrollbar.configure(command = monthly_listbox.yview)
        vertical_scrollbar.grid(row = 1, column = 1, sticky = "ns")
        monthly_listbox.configure(yscrollcommand = vertical_scrollbar.set)

        #Add items to listbox
        date_dict = {}
        for row in dataset1:
            date = row["award_date"].split("-")
            #Check for valid date format
            if len(date) != 3:
                date = row["award_date"].split("/")
                if len(date) != 3:
                    continue
                if len(date[1]) == 1:
                    date[1] = "0" + date[1]
                if date[2] + "/" + date[1] not in date_dict:
                    date_dict[date[2] + "/" + date[1]] = float(row["awarded_amt"])
                else:
                    date_dict[date[2] + "/" + date[1]] += float(row["awarded_amt"])
            else:
                if date[0] + "/" + date[1] not in date_dict:
                    date_dict[date[0] + "/" + date[1]] = float(row["awarded_amt"])
                else:
                    date_dict[date[0] + "/" + date[1]] += float(row["awarded_amt"])

        #Sort procurements by date
        sorted_date_keys = sorted(date_dict.keys())
        sorted_values = []
        for key in sorted_date_keys:
            monthly_listbox.insert(END, (key + "       " + str(date_dict[key])))
            sorted_values.append(date_dict[key])

        #Create procurement predictions
        num_predictions = 12
        prediction_dict = {}
        current_date = sorted_date_keys[len(sorted_date_keys) - 1]
        prediction_dict[current_date] = date_dict[current_date]
        #Get procurements from last 12 months
        previous_procurements = []
        if(len(sorted_values) < 12):
            print "Unable to make prediction, at least 12 months of data required"
            return frame

        for i in range(13):
            previous_procurements.append(sorted_values[-i])
        previous_procurements.reverse()

        for i in range(num_predictions):
            current_date = self.get_next_month_string(current_date)

            #Calculate average in last 12 months, after removing outlier values
            #Outliers are found based on the interquartile range of the previous procurements
            difference = 0
            previous_procurements_sorted = sorted(previous_procurements)
            lower_q = (previous_procurements_sorted[2] + previous_procurements_sorted[3]) / 2
            higher_q = (previous_procurements_sorted[8] + previous_procurements_sorted[9]) / 2
            inter_q = higher_q - lower_q
            low_val = lower_q - (1.5 * inter_q)
            high_val = higher_q + (1.5 * inter_q)
            total = 0
            total_count = 0
            for pro in previous_procurements_sorted:
                if pro >= low_val and pro <= high_val:
                    total += pro
                    total_count += 1
            #Calculate current month prediction
            prediction_dict[current_date] = (total + (previous_procurements[0] * 6)) / (total_count + 6)

            previous_procurements.pop(0)
            previous_procurements.append(prediction_dict[current_date])

        sorted_prediction_keys = sorted(prediction_dict.keys())
        sorted_prediction_values = []
        for key in sorted_prediction_keys:
            sorted_prediction_values.append(prediction_dict[key])

        #Create graph
        f = Figure(figsize = (12,5), dpi = 60)
        a = f.add_subplot(111)
        #Populate initial values of graph
        a.plot(sorted_date_keys, sorted_values, color = '#61ff7e', label='Past procurement')
        #Populate prediction values of graph
        a.plot(sorted_prediction_keys, sorted_prediction_values, color = '#5bbffc', label='Predicted procurement')
        #Style graph
        a.set_title("Monthly Procurement")
        a.set_xlabel("Month")
        a.set_ylabel("Procurement(Dollars)")
        a.legend()
        a.tick_params(axis = 'x',labelsize = 10, labelrotation = 90)
        f.tight_layout(pad = 0)

        #Create canvas for graph to be displayed using TKinter
        canvas = FigureCanvasTkAgg(f, frame)
        canvas.draw()
        canvas.get_tk_widget().grid(row = 0, column = 1, sticky = "nw")

        return frame

    def get_next_month_string(self, date):
        """Returns the following month of the specified date as a string

        Parameters:
        date (string):    The current date
        """
        date = date.split("/")
        if int(date[1]) > 11:
            new_month = 1
            new_year = int(date[0]) + 1
        else:
            new_month = int(date[1]) + 1
            new_year = int(date[0])
        if len(str(new_month)) == 1:
            return str(new_year) + "/0" + str(new_month)
        else:
            return str(new_year) + "/" + str(new_month)

    def list_expired_contractors(self, dataset2):
        """List down the expired and non-expired contractors, sorting from the earliest (Function 11)

        Parameters:
        dataset2 (list):    The listing of registered contractors data set
        """
        import datetime
        today = datetime.date.today()
        today = today.strftime("%d/%m/%Y")

        from datetime import datetime
        def get_key(date):
            return datetime.strptime(date, "%d/%m/%Y")

        datetime1 = get_key(today)

        expiry=[]
        active=[]

        for row in dataset2:
            if datetime1 >= get_key(row["expiry_date"]):
                expiry.append(row)
            else:
                active.append(row)

        expiry.sort(key= lambda row: get_key(row["expiry_date"]))
        active.sort(key=lambda row: get_key(row["expiry_date"]))
        return expiry,active

    def list_contractorcount_by_workhead(self, dataset2):
        """List down the contractor count by individual workhead. (Function 9)
                 Parameters:
                 dataset2 (list):    The listing of registered contractors data set
         """
        list1=[]
        for item in dataset2:
            list1.append(item['workhead'])  # extracts all the values under workhead column

        workhead_dict={}
        for workhead in list1:
            freq=str(list1.count(workhead))  # counts frequency of each workhead
            workhead_dict[workhead]=freq   # frequency of workhead value is matched to the its workhead in a dictionary

        #sorts workhead_dict by key. OrderedDict datatype is used to allow rearranging of dictionary order
        sorted_workhead_dict = collections.OrderedDict(sorted(workhead_dict.items(), key=operator.itemgetter(0)))

        workheadlist= [{"Workhead" : key, "Count": value} for key, value in sorted_workhead_dict.items()]
        return workheadlist

    def list_contractorcount_by_grade(self, dataset2):
        """List down the number of contractors by grade (Function 10)
                 Parameters:
                 dataset2 (list):    The listing of registered contractors data set
         """
        list1=[]
        for item in dataset2:
            list1.append(item['grade'])  # extracts all the values under grade column

        grades_dict={}
        for grade in list1:
            freq=str(list1.count(grade))  # counts frequency of each grade
            grades_dict[grade]=freq   # frequency of grade value is matched to the its grade in a dictionary

        #sorts grades_dict by key. OrderedDict datatype is used to allow rearranging of dictionary order
        sorted_grades_dict = collections.OrderedDict(sorted(grades_dict.items(), key=operator.itemgetter(0)))

        gradeslist = [{"Grade" : key, "Count": value} for key, value in sorted_grades_dict.items()]
        return gradeslist
